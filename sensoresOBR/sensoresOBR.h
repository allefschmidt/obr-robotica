#include <Arduino.h>

#ifndef sensoresOBR_h
#define sensoresOBR_h

#include "Arduino.h"

class sensoresOBR {
public:
//	sensoresOBR(int trigger, int echo, int s0, int s1, int s2, int s3, int out);  //Constructor para motores e Faixa
	sensoresOBR(int trigger, int echo);  //Constructor para motores e Faixa

	int valorUltrassonico();
	
	//Fun��es referentes a detec��o de cores
	bool detectaVerde();
	bool detectaCinza(int verm, int azu, int verd);
	int valorVerde();
	int valorAzul();
	int valorVermelho();
	
private: //At� o momento creio eu que isso s�o vari�veis globais, a serem utilizadas no arquivo .cpp
	uint8_t control_trigger;
	uint8_t control_echo;

	uint8_t control_out;
	uint8_t control_s1;
	uint8_t control_s2;
	uint8_t control_s3;
};

#endif
