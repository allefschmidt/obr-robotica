#include <Arduino.h>
#include <Ultrasonic.h>
#include "sensoresOBR.h"


//sensoresOBR::sensoresOBR(int trigger, int echo, int s0, int s1, int s2, int s3, int out){
sensoresOBR::sensoresOBR(int trigger, int echo){
	control_trigger = trigger;
	control_echo = echo;
		
/*	control_out = out;
	control_s1 = s1;
	control_s2 = s2;
	control_s3 = s3;

	//Variaveis referentes aos pinos do sensor de COR
	pinMode(s0, OUTPUT);
	pinMode(s1, OUTPUT);
	pinMode(s2, OUTPUT);
	pinMode(s3, OUTPUT);
	pinMode(out, INPUT);
	
 	digitalWrite(s0, HIGH);
  	digitalWrite(s1, LOW);
*/

}
 
int sensoresOBR::valorUltrassonico(){ //Sensor ultrass�nico n�o tem pinMode setado, o que � maravilhoso, podemos utilizar como fun��o
	int centimetros; // � um inteiro pois o valor obtido pela fun��o convert() � double, logo ser� transformado em inteiro:. 6.2 (double) = 6 (int)
	long microsec;
	Ultrasonic ultrasonic(control_trigger, control_echo); //Fun��o que controi o objeto ultrasonic do tipo Ultrasonic, utilizando valores do construtor
	microsec = ultrasonic.timing(); 
	centimetros = ultrasonic.convert(microsec, Ultrasonic::CM); //Fun�a� de convers�o 
	

	if (centimetros > 600) {
		centimetros = ultrasonic.convert(microsec, Ultrasonic::CM); // para evitar valores absurdos
		return centimetros;
	} else{
		return centimetros; //Retorno do valor convertido
	}
}

bool sensoresOBR::detectaVerde(){
	int green = valorVerde();
	int blue = valorAzul();
	int red = valorVermelho();
	if (green < red && green < blue) //Caso as condi��es dadas pelo sensor para decte��o do verde forem atingida
  	{
    return true; 
	} else{
	return false;
	}
}

bool sensoresOBR::detectaCinza(int verde,int azul, int vermelho){
	int green = valorVerde();
	int blue = valorAzul();
	int red = valorVermelho();
	if (green > verde && blue > azul && red < vermelho) //Caso as condi��es dadas pelo sensor para decte��o do cinza forem atingida, necessario definir
  	{
    return true; 
	} else{
	return false;
	}
}
int sensoresOBR::valorAzul(){
	int blueColor = 0;
	// Setting BLUE (B) filtered photodiodes to be read
	digitalWrite(control_s2,LOW);
	digitalWrite(control_s3,HIGH);
	
	// Reading the output frequency
	int blueFrequency = pulseIn(control_out, LOW);
	// Remaping the value of the BLUE (B) frequency from 0 to 255
	// You must replace with your own values. Here's an example: 
	 blueColor = map(blueFrequency, 38, 84, 255, 0);
	//blueColor = map(blueFrequency, XX, XX, 255, 0);
	return blueColor;
		
}

int sensoresOBR::valorVerde(){
	int greenColor = 0;
	digitalWrite(control_s2,HIGH);
  	digitalWrite(control_s3,HIGH);
  
  // Reading the output frequency
  int greenFrequency = pulseIn(control_out, LOW);
  // Remaping the value of the GREEN (G) frequency from 0 to 255
  // You must replace with your own values. Here's an example: 
    greenColor = map(greenFrequency, 100, 199, 255, 0);
  //greenColor = map(greenFrequency, XX, XX, 255, 0);
  
  return greenColor;
}
 
int sensoresOBR::valorVermelho(){
	int redColor = 0;
		 // Setting RED (R) filtered photodiodes to be read
  digitalWrite(control_s2,LOW);
  digitalWrite(control_s3,LOW);
  
  // Reading the output frequency
  int redFrequency = pulseIn(control_out, LOW);
  // Remaping the value of the RED (R) frequency from 0 to 255
   redColor = map(redFrequency, 70, 120, 255,0);
  //redColor = map(redFrequency, XX, XX, 255,0);
  
  return redColor;
}



/*bool sensoresOBR::detectaVerde(){
	//Rotina que le o valor das cores
  digitalWrite(control_s2, LOW);
  digitalWrite(control_s3, LOW);
  //count OUT, pRed, RED
  red = pulseIn(control_out, digitalRead(control_out) == HIGH ? LOW : HIGH);
  digitalWrite(control_s3, HIGH);
  //count OUT, pBLUE, BLUE
  blue = pulseIn(control_out, digitalRead(control_out) == HIGH ? LOW : HIGH);
  digitalWrite(s2, HIGH);
  //count OUT, pGreen, GREEN
  green = pulseIn(control_out, digitalRead(control_out) == HIGH ? LOW : HIGH);
  delay(10);
	if (green < red && green < blue) //Caso as condi��es dadas pelo sensor para decte��o do verde forem atingida
  	{
    return true; 
	} else{
	return false;
	}
}; */

