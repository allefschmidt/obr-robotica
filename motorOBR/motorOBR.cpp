#include <Arduino.h>
#include <Ultrasonic.h>
#include "motorOBR.h"


// Construtor, chamado na inicializacao da biblioteca/classe
motorOBR::motorOBR(int in1, int in2, int in3, int in4, int sensorEsquerda, int sensorDireita) {
  
pinMode(in1, OUTPUT); // MOTOR ESQUERDO - 
pinMode(in2, OUTPUT); // MOTOR ESQUERDO -
pinMode(in3, OUTPUT); // MOTOR DIREITA - 
pinMode(in4, OUTPUT); // MOTOR DIREITA -

control_in1 = in1; 
control_in2 = in2;
control_in3 = in3;
control_in4 = in4;
control_faixa_esquerda = sensorEsquerda;
control_faixa_direita = sensorDireita;

pinMode(sensorEsquerda, INPUT);
pinMode(sensorDireita, INPUT);


}
bool motorOBR::valorEsquerdo(){
	bool valorEsquerda = digitalRead(control_faixa_esquerda);
	return valorEsquerda;	
}
bool motorOBR::valorDireito(){
	bool valorDireita = digitalRead(control_faixa_direita);
	return valorDireita;
}
void motorOBR::segueFaixa(int tempoFrente, int tempoVirar){

int valueEsquerdo = valorEsquerdo();
int valueDireito = valorDireito();

	if (valueEsquerdo == 0 && valueDireito == 0){
	  andarFrente(tempoFrente);
	  pararMotores(5);
	} 	else if (valueEsquerdo == 0 && valueDireito == 1) {
		virarDireita(tempoVirar);
		} 	else if (valueEsquerdo == 1 && valueDireito == 0) {
		virarEsquerda(tempoVirar);
			} else {
		andarFrente(tempoFrente);
}
}

void motorOBR::testeMotores(int tempo) {
	
	andarFrente(tempo);
	pararMotores(tempo);
	andarTras(tempo);
	pararMotores(tempo);
	virarDireita(tempo);
	pararMotores(tempo);
	virarEsquerda(tempo);
	pararMotores(tempo);

}
void motorOBR::andarFrente(int tempo){ //Varivel tempo dever ser alterada para verificar qual o melhor comportamento.
  // Esta funo faz com que os motores girem no sentido HORRIO. 
  // Ligando o motor ESQUERDO - HORAIO
  digitalWrite(control_in1, HIGH);
  digitalWrite(control_in2, LOW);
  // Ligando o motor DIREITO - HORARIO
  digitalWrite(control_in3, HIGH);
  digitalWrite(control_in4, LOW);
  delay(tempo);
 // pararMotores(tempo); TO DO -> Testar

}
void motorOBR::andarTras(int tempo){ //Varivel tempo dever ser alterada para verificar qual o melhor comportamento.
	digitalWrite(control_in1, LOW);
	digitalWrite(control_in2, HIGH);
	digitalWrite(control_in3, LOW);
	digitalWrite(control_in4, HIGH);
	delay(tempo);
	// pararMotores(tempo);
}
void motorOBR::pararMotores(int tempo){
  digitalWrite(control_in1, LOW);
  digitalWrite(control_in2, LOW); 
  digitalWrite(control_in3, LOW);
  digitalWrite(control_in4, LOW);
  delay(tempo);
}
void motorOBR::virarEsquerda(int tempo){
	 // Ligando o motor ESQUERDO - ANTI-HORRIO.
  digitalWrite(control_in1, LOW);
  digitalWrite(control_in2, HIGH);
  // Ligando o motor DIREITO - HORRIO
  digitalWrite(control_in3, HIGH);
  digitalWrite(control_in4, LOW);
  delay(tempo);
}
void motorOBR::virarDireita(int tempo){
	 // Ligando o motor ESQUERDO - HORRIO
  digitalWrite(control_in1, HIGH);
  digitalWrite(control_in2, LOW);
  // Ligando o motor DIREITO - ANTI-HORRIO
  digitalWrite(control_in3, LOW);
  digitalWrite(control_in4, HIGH);
  delay(tempo);
}

void motorOBR::desvioDireita(int tempoParado,int direita, int esquerda,int frente,int ultimo){
	pararMotores(tempoParado);
	andarTras(100);
	pararMotores(tempoParado);
	virarDireita(direita);
	pararMotores(tempoParado);
	andarFrente(frente);
	pararMotores(tempoParado);
	virarEsquerda(esquerda);
	pararMotores(tempoParado);
	andarFrente(frente);
	pararMotores(tempoParado);
	virarEsquerda(esquerda);
	pararMotores(tempoParado);
	andarFrente(frente);
	pararMotores(tempoParado);
	virarDireita(ultimo);
}

void motorOBR::desvioEsquerda(int tempoParado,int direita, int esquerda,int frente,int ultimo){
	pararMotores(tempoParado);
	andarTras(100);
	pararMotores(tempoParado);
	virarEsquerda(direita);
	pararMotores(tempoParado);
	andarFrente(frente);
	pararMotores(tempoParado);
	virarDireita(esquerda);
	pararMotores(tempoParado);
	andarFrente(frente);
	pararMotores(tempoParado);
	virarDireita(esquerda);
	pararMotores(tempoParado);
	andarFrente(frente);
	pararMotores(tempoParado);
	virarDireita(ultimo);
}

void motorOBR::encontrouCinza(bool estado){
	if (estado == true){
		salaCentro();
		salaMeio();
	}
}


int motorOBR::ultrassonicoVitima(int trigger, int echo){
	int centimetros; // � um inteiro pois o valor obtido pela fun��o convert() � double, logo ser� transformado em inteiro:. 6.2 (double) = 6 (int)
	long microsec;
	Ultrasonic ultrasonic(trigger, echo); //Fun��o que controi o objeto ultrasonic do tipo Ultrasonic, utilizando valores do construtor
	microsec = ultrasonic.timing(); 
	centimetros = ultrasonic.convert(microsec, Ultrasonic::CM); //Fun�a� de convers�o 
	

	if (centimetros > 600) {
		centimetros = ultrasonic.convert(microsec, Ultrasonic::CM); // para evitar valores absurdos
		return centimetros;
	} else{
		return centimetros; //Retorno do valor convertido
	}
}
void motorOBR::radarVitima(int tempoParado, int tempoGirando, int tempoFrente, int trigger, int echo){
	int varTempo = 0;
	int valorUltrassonico = ultrassonicoVitima(trigger, echo);
	
	do {
	virarDireita(tempoGirando);
	pararMotores(tempoParado);
	valorUltrassonico = ultrassonicoVitima(trigger, echo);
	} while(valorUltrassonico > 40);
	
	if (valorUltrassonico < 40){
		andarFrente(tempoFrente);
	}

}

