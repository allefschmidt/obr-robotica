#include <Arduino.h>

#ifndef motorOBR_h
#define motorOBR_h

#include "Arduino.h"

class motorOBR
{
  public:
  	//Fun��es referentes ao movimento do motor
    motorOBR (int in1,int in2,int in3,int in4, int sensorEsquerda, int sensorDireita);
    
	//Fun��es referentes aos motores
	void testeMotores(int tempo);
    void andarFrente(int tempo);
    void andarTras(int tempo);
    void pararMotores(int tempo);
    void virarEsquerda(int tempo);
    void virarDireita (int tempo);
    //Fun��es referentes ao seguidor de faixa
    bool valorEsquerdo();
    bool valorDireito();
    void segueFaixa(int tempoFrente, int tempoVirar);
    //Fun��es referentes aos desvios de obstaculos
    void desvioEsquerda(int tempoParado,int direita, int esquerda,int frente, int ultimo);
    void desvioDireita(int tempoParado,int direita, int esquerda,int frente, int ultimo);
    
    //Fun��es referentes ao alinhamento do rob�;
    void encontrouCinza(bool estado);
    void salaCentro();
	void salaMeio();
	int ultrassonicoVitima(int trigger, int echo);
    //bool alinharCentro(int valorUltrassonico);
	
	void radarVitima( int tempoParado, int tempoGirando, int tempoFrente, int trigger, int echo); //Deve testar giroCompleto

    
  private:
    uint8_t control_in1;
    uint8_t control_in2;
    uint8_t control_in3;
    uint8_t control_in4;
    
    uint8_t control_faixa_esquerda;
    uint8_t control_faixa_direita;
};

#endif
