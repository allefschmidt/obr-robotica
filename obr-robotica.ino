#include <motorOBR.h>
#include <sensoresOBR.h>

//Pinos dos sensores e motores, cuidado ao mexer aqui.
#define motor_in1 9 //Motor A - 9 
#define motor_in2 8//Motor A + 8
#define motor_in3 7 //Motor B - 7
#define motor_in4 6 //Motor B + 6

//Pino dos sensores de faixa
#define sensor_Fesquerda 45
#define sensor_Fdireita 44

//Pino dos sensores ultrassonicos
#define trigger 52
#define echo 53

motorOBR obrx(motor_in1,motor_in2,motor_in3,motor_in4,sensor_Fesquerda,sensor_Fdireita);
sensoresOBR sensor(trigger,echo);

void  setup() {
} 

void loop() {
//obrx.testeMotores(1000); //Utilize essa função caso tenha dúvida do funcionamento dos motores.
//Lembre-se, mal comportamento nos movimentos pode ser bateria.
sala1Sala2();
//vai dar certo
}

void desvioDireita(int parado){ //Parar motores, adicione o tempo que o robô permanece parado
  obrx.pararMotores(parado);
  obrx.andarTras(100);
  obrx.pararMotores(parado);
  obrx.virarDireita(100); //Aqui que começa a brincadeira, necessário procurar valores ideais.
  obrx.pararMotores(parado);
  obrx.andarFrente(100);
  obrx.pararMotores(parado);
  obrx.virarEsquerda(100);
  obrx.pararMotores(parado);
  obrx.andarFrente(100);
  obrx.pararMotores(parado);
  obrx.virarEsquerda(100);
  obrx.pararMotores(parado);
  obrx.andarFrente(100);
  obrx.pararMotores(parado);
  obrx.virarDireita(100); //Último movimento que fará o robô retornar a linha
}

void sala1Sala2(){
int valorUltra = sensor.valorUltrassonico();

if (valorUltra < 15) { //Aqui define qual a melhor distancia que o robô irá detectar o obstaculo
  desvioDireita(100); //o valor inserido se refere ao tempo que o robo permanace parado
} else {
  obrx.segueFaixa(10,120); //Tempo andando pra frente e tempo virando para os lados
}
}

